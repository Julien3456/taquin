let picture = new display();
let settings = new Settings(picture);
let result = new Result(0, 0);

function changeLevel(choice){
    settings.setLevel(choice);
    createTabSections();
    picture.modifyDisplay();
}
function changeMove(){
    settings.setMove(document.getElementById('toggle1').checked);
}
function changeHelp(){
    settings.setHelp(document.getElementById('toggle2').checked);
    picture.modifyDisplay();
}
function createTabSections(){
    Section.createTabSections(picture);
}
function displayPictures(){
    picture.displayTaquin();
}

function start(){
    createTabSections();
    displayPictures();
    result.showResult();
}

function restart(){
    settings.disableDifficulty(false);
    settings.init();
    settings = null;
    picture = null;
    result = null;
    picture = new display();
    settings = new Settings(picture);
    result = new Result(0, 0);
    start();
}

document.addEventListener("DOMContentLoaded", () => {
    start();

/******************    Section Start    ******************/
    document.getElementById('start').addEventListener('click', () => {

        settings.start = true;
        // On bloque le changement de niveau
        settings.disableDifficulty(true);

    // On brouille le taquin, on vérifie qu'il soit faisable et on affiche le taquin brouillé
        do {
            settings.mixSection();
            settings.mixDegree(settings.tabSections, result)
        }
        while (result.mixometer%2 !== 0);
        picture.modifyDisplay();
        result.showResult();


/******************    Section Jeux    ******************/
        document.getElementById('canvas').addEventListener('click', (event) => {
            if (settings.start === true) {
                // On récupère la localisation du click
                let click = new Click((event.pageX - event.target.offsetLeft) * picture.ratio,
                    (event.pageY - event.target.offsetTop) * picture.ratio);

                // On trouve la section associé à la position du click
                click.findPos();

                // On trouve l'Id de la section de destination en fonction du mouvement
                click.section.move(click);

                // On affiche le taquin modifié, on ajoute un click au compteur et on recalcul le mixomètre
                picture.modifyDisplay();
                result.addClick();
                settings.mixDegree(settings.tabSections, result);

                // Affichage du resultat
                result.showResult();

                click = null;

                if (result.mixometer === 0 &&
                    settings.tabSections[settings.nbSections - 1].idZone === settings.nbSections - 1) {
                    picture.displayWin();
                }
            }
        })
    })
    document.getElementById('relaunch').addEventListener('click', () => {
        restart();
    })
    document.addEventListener('keyup', (event) => {
        if (event.code === "Space") restart();
    })
})