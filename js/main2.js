document.addEventListener('DOMContentLoaded', () => {
    /*let canvasWidth = 800;
    let canvasHeight = 800;
    const canvas = document.getElementById("canvas");
    canvas.width = canvasWidth;
    canvas.height = canvasHeight;
    let ctx = document.getElementById('canvas').getContext('2d');
    const picture = document.createElement("img");
    const miniature = document.createElement("img");
    picture.src = findPictures();
    picture.id = "scream";
    picture.alt = "taquin/'s picture";
    picture.width = 800;
    picture.height = 800;
    miniature.alt = "miniature/'s taquin";
    miniature.src = picture.src
    miniature.id = "miniature";
    const start = document.getElementById('start');
    const display = document.getElementById('click');
    const mix = document.getElementById('mix');
    const selectLevel = document.getElementById('option-window')
    const taquin = document.getElementById('taquin');
    const miniFrame = document.getElementById('frame')
    const relaunch = document.getElementById('relaunch');
    const switchMove = document.getElementById('toggle1')
    const switchHelp = document.getElementById('toggle2')*/

    taquin.appendChild(picture);
    miniFrame.appendChild(miniature);

    const selectDif = document.getElementsByClassName('difficulty');
    let difficulty = 3;
    let maxSection;
    let ratio=4/difficulty;
    let gridTab = [];
    let gridRand = [];
    let nbClic = 0;
    let mixD;
    let clicStart = 0;
    let optionHighLength = 0;
    let optionPosition = 0;

// Fonction pour initialiser et affichager l'image non brouillée
    window.onload = function () {
        ratio = 4 / difficulty;
        maxSection = Math.pow(difficulty, 2);
// création du tableau de référence inital
        for (let i = 1; i < maxSection; i++) {
            gridTab.push(i);
        }
        gridTab.push(0);
// Affichage du taquin dans l'ordre avec le tableau de référence
        displayTaquin(canvas, picture, difficulty, ratio, gridTab);
        createGrid(canvas, difficulty, ratio);
        miniature.src = picture.src;

    }

// Selection du niveau de difficulté
    selectLevel.addEventListener('click', () => {
        difficulty = parseInt(difficultyLevel(selectDif));
        ratio=4/difficulty;
        maxSection = Math.pow(difficulty, 2);

        gridTab = [];
// création du tableau de référence inital
        for (let i = 1; i < maxSection; i++) {
            gridTab.push(i);
        }
        gridTab.push(0);

        maxSection = Math.pow(difficulty, 2);
        displayTaquin(canvas, picture, difficulty, ratio, gridTab);
        createGrid(canvas, difficulty, ratio);
        clearSection(canvas, difficulty -1, difficulty -1, ratio);
    })

// Si il y a un evenement clic dans le bouton start
    start.addEventListener('click', () => {

// Vérification d'un taquin possible
        while (mixD%2 !== 0){
            gridRand = tabRandom(maxSection);
            mixD = mixDegree(gridRand);
        }
// Affichage du mixDegree
        displayMix(mix, mixD);

// Afficher le taquin brouillé
        displayTaquin(canvas, picture, difficulty, ratio, gridRand);

// Ajout du quadrillage
        createGrid(canvas, difficulty, ratio);

        clicStart = 1;
    })

// Si il y a un evenement clic dans le canvas
    canvas.addEventListener('click', (event) => {
        if (clicStart) {
    // Variable indiquant le ratio dû au responsive
            let ratioResp = canvas.clientWidth / canvasWidth
    // Variable indiquant la coordonnée X sur le canvas moins la marge
            let posX = event.pageX - (canvas.offsetLeft + 5);
    // Variable indiquant la coordonnée Y sur le canvas moins la marge
            let posY = event.pageY - (canvas.offsetTop + 5);
    // Variable indiquant la position de la case selectionné dans le tableau
            let grid = ((Math.floor((posY) / ((200 * ratio) * ratioResp)) * difficulty) + ((Math.floor((posX) / ((200 * ratio) * ratioResp))) + 1)) - 1;

            const sectionsRow = [];
            const sectionsRowValue = [];
            const sectionsCol = [];
            const sectionsColValue = [];

            let startRow = grid-(Math.floor(posX / ((200 * ratio) * ratioResp)));
            let startCol = grid - (difficulty * Math.floor(posY / ((200 * ratio) * ratioResp)));
            for (let i = 0; i < difficulty; i++){
                sectionsRow.push(startRow);
                sectionsRowValue.push(gridRand[startRow]);
                sectionsCol.push(startCol);
                sectionsColValue.push(gridRand[startCol]);
                startRow += 1;
                startCol += difficulty;
            }

// Si l'utilisateur bascule l'option de grnad déplacement
            switchMove.addEventListener('click', (event) => {
                if (event.target.checked) optionHighLength = 1
                else optionHighLength = 0;
            })
// Si l'utilisateur bascule l'option d'aide au positionnement
            switchHelp.addEventListener('click', (event) => {
                if (event.target.checked){
                    optionPosition = 1;
                    displayCadreSection(canvas, difficulty, maxSection, gridRand)
                }
                else optionPosition = 0;
            })

// Si le clic est sur le taquin et la section libre est sur le même axe X
            if (0 < grid < Math.pow(difficulty, 2) && sectionsRowValue.includes(0)) {
                let indexSource = sectionsRow.findIndex((element) => element === grid);
                let indexDest = sectionsRowValue.findIndex((element) => element === 0);
                let dif = indexDest - indexSource

                if (optionHighLength || (optionHighLength === 0 && (dif === 1 || dif === -1))) {
                    nbClic++;
                    gridRand.splice(gridRand.findIndex((element) => element === 0), 1)
                    gridRand.splice(grid, 0, 0)
                    displayTaquin(canvas, picture, difficulty, ratio, gridRand);
                }
            }

// Si le clic est sur le taquin et la section libre est sur le même axe Y
            if (0 < grid < Math.pow(difficulty, 2) && sectionsColValue.includes(0)) {
                let indexSource = sectionsCol.findIndex((element) => element === grid);
                let indexDest = sectionsColValue.findIndex((element) => element === 0);
                let dif = indexDest - indexSource
                if (optionHighLength || (optionHighLength === 0 && (dif === 1 || dif === -1))) {
                    if (dif < 0){
                        dif *= -1;
                        for (let i = indexDest; i < indexSource; i++) {
                            let pos = sectionsCol[i];
                            let posValue = sectionsColValue[i+1]
                            gridRand.splice(pos, 1, posValue)
                        }
                        gridRand.splice(grid, 1, 0)
                    } else {
                        for (let i = dif+indexSource; i > indexSource; i--) {
                            let pos = sectionsCol[i];
                            let posValue = sectionsColValue[i-1]
                            gridRand.splice(pos, 1, posValue)
                        }
                        gridRand.splice(grid, 1, 0)
                    }
                    nbClic++;
                    console.log(gridRand[maxSection-1], maxSection-1);
                    displayTaquin(canvas, picture, difficulty, ratio, gridRand);
                }
            }

// Affichage de la mise à jour du compteur de clic
            displayClic(display, nbClic);

// Controle et affichage du nouveau niveau de mixDegree
            mixD = mixDegree(gridRand);
            displayMix(mix, mixD);

// Si le taquin est résolu, affichage du text
            if (mixD === 0 && gridRand[maxSection-1] === 0) {
                displayWin(ctx, canvasWidth, canvasHeight);
            }
// Sinon affichage du quadrillage et on continue
            else {
                createGrid(canvas, difficulty, ratio);
// Affichage d'un cadre sur les sections bien positionnées si l'option est activée
                if (optionPosition) { displayCadreSection(canvas, difficulty, maxSection, gridRand); }
            }
        }
    })

// Refresh du taquin si appuie sur la barre espace
    document.addEventListener('keyup', (event) => {
        if (event.keyCode === 32) {
            refresh();
            createGrid(canvas, difficulty, ratio);
        }
    })

// Refresh du taquin si appuie sur le bouton dedié
    relaunch.addEventListener('click', () => {
        refresh();
        createGrid(canvas, difficulty, ratio);
    })

    function refresh(){
        gridRand = [];
        nbClic = 0;
        mixD = 1;
        clicStart = 0;
        displayTaquin(canvas,picture, difficulty, ratio, gridTab);
        displayClic(display, nbClic);
        displayMix(mix, 0)
    }
})


/****************************************************************************************
 * Fonction pour afficher le nouvel emplacement libre
 * @param canvas
 * @param srcX
 * @param srcY
 * @param ratio
 */
function clearSection(canvas, srcX, srcY, ratio){
    canvas.getContext("2d").clearRect(srcX * (200*ratio), srcY * (200*ratio), (200*ratio), (200*ratio));
}

/****************************************************************************************
 * Fonction rechercher des images aléatoirement
 * @returns {string}
 */
function findPictures(){
    let nbImg = Math.floor((Math.random() * 10)+1);
    return `images/gamePictures/image${nbImg}.jpg`;
}

/*****************************************************************************************
 * Affichage du taquin en fonction du tableau de reference
 * @param canvas
 * @param picture
 * @param difficulty
 * @param ratio
 * @param gridTab
 */
function displayTaquin(canvas, picture, difficulty, ratio, gridTab) {
    let i = 0;
    for (let indexY = 0; indexY < difficulty; indexY++) {
        for (let indexX = 0; indexX < difficulty; indexX++) {
    // Variable indiqant la coordonnée X0 sur l'image de reference de la section selectionnée en fonction du ratio de difficulté
            let srcImgX = ((gridTab[i] - 1) % difficulty);
    // Variable indiqant la coordonnée Y0 sur l'image de reference de la section selectionnée en fonction du ratio de difficulté
            let srcImgY = Math.floor((gridTab[i] - 1) / difficulty);

// Si le numéro de section est différent de 0, on affiche la section a son emplacement brouillé
            if (gridTab[i] !== 0) {
                displaySection(canvas, picture, difficulty, srcImgX, srcImgY, indexX, indexY);
// Sinon on affiche l'emplacement vide
            } else {
                clearSection(canvas, indexX, indexY, ratio);
            }
            i++;
        }
    }
}

/***************************************************************************************************
 * Fonction pour afficher les sections
 * @param canvas
 * @param image
 * @param difficulty
 * @param xSrc
 * @param ySrc
 * @param xDest
 * @param yDest
 */
function displaySection(canvas, image, difficulty, xSrc, ySrc, xDest, yDest) {
    let ratio=4/difficulty;
    canvas.getContext("2d").drawImage(image, xSrc * (200*ratio), ySrc * (200*ratio), (200*ratio), (200*ratio), xDest * (200*ratio), yDest * (200*ratio), (200*ratio), (200*ratio));
}

/************************************************************************************************
 * Fonction pour afficher un cadre sur les sections bien positionnées
 * @param canvas
 * @param difficulty
 * @param maxSection
 * @param gridRand
 */
function displayCadreSection(canvas, difficulty, maxSection, gridRand) {
    let ratio=4/difficulty;

    for (let i = 1; i < maxSection; i++) {
        if (i === gridRand[i-1]) {
            let srcImgX = (i-1) % difficulty;
            let srcImgY = Math.floor((i-1) / difficulty);
            canvas.getContext("2d").strokeStyle = 'rgb(34,255,0)';
            canvas.getContext("2d").strokeRect(srcImgX * (200*ratio), srcImgY * (200*ratio), (200*ratio), (200*ratio));
        }
    }
}

/*****************************************************************************************************
 * Fonction pour créer le quadrillage sur le canvas
 * @param canvas
 * @param caseNumber
 * @param ratio
 */
function createGrid(canvas, caseNumber, ratio) {
    for (let x = 1; x < caseNumber; x++) {

        let ctx = canvas.getContext('2d');
        ctx.lineWidth = 5;
        ctx.strokeStyle = 'rgb(113,181,220)';
        ctx.beginPath();
        ctx.moveTo(x * (200 * ratio), 0);
        ctx.lineTo(x * (200 * ratio), 800);
        ctx.moveTo(x * (200 * ratio), 0);

        ctx.moveTo(0, x * (200 * ratio));
        ctx.lineTo(800, x * (200 * ratio));
        ctx.moveTo(0, x * (200 * ratio));

        ctx.stroke();
    }
}

/*********************************************************************************************************
 * fonction générant un tableau de numéros de section en vérifiant le non duplicat
 * @param maxSection
 * @returns {[]}
 */
function tabRandom(maxSection){
    let nbSection;
    let gridRand = [];
    for (let i = 0; i < maxSection -1; i++){
        do {
            nbSection = nbRandomSection(maxSection -1);
        }
        while (gridRand.find(section => section === nbSection) && nbSection !== 0);
        gridRand.push(nbSection);
    }
// Ajout d'une section vide dans le tableau
    gridRand.push(0);
    return gridRand;
}

/*******************************************************************************************************
 * fonction générant des nombres aléatoirement entre 1 et le max en paramètre
 * @param max
 * @returns {*}
 */
function nbRandomSection(max){
    return Math.floor((Math.random() * max)+1);
}

/*******************************************************************************************************
 * fonction affichant le nombre de clic dans un input
 * @param input
 * @param clic
 */
function displayClic(input, clic){
    input.value = clic;
}

/*******************************************************************************************************
 * fonction recherchant le niveau de difficulté slectionné
 * @param selectDif
 * @returns {*}
 */
function difficultyLevel(selectDif){
    let difficulty;
    Array.prototype.filter.call(selectDif, function (element){
        if (element.checked){ difficulty = element.id }
    })
    return difficulty;
}

/********************************************************************************************************
 * fonction affichant le mélangeomèrte dans un input
 * @param input
 * @param mix
 */
function displayMix(input, mix){
    input.value = mix;
}

/**********************************************************************************************************
 * fonction retournant une variable indiquant le mélangeomètre
 * @param tab
 * @returns {number}
 */
function mixDegree(tab){
    let i = 0;
    let counter = 0;
    tab.forEach(function (element){
        i++
        for (let e = i; e < tab.length; e++) {
            if (element > tab[e] && element !== 0 && tab[e] !== 0){
                counter++
            }
        }
    })
    return counter;
}

/********************************************************************************************************
 * Fonction d'affichage du message de réussite
 * @param ctx
 * @param width
 * @param height
 */
function displayWin(ctx, width, height){
    ctx.font = 'bold 64px sans-serif';
    ctx.fillStyle = '#000';
    ctx.textAlign = 'center';
    ctx.textBaseline = 'middle';
    ctx.strokeStyle = 'white';
    ctx.lineWidth = 5;
    ctx.strokeText('Retour à la normal', width / 2, height - 600);
    ctx.fillText('Retour à la normal', width / 2, height - 600);
    ctx.font = 'bold 30px sans-serif';
    ctx.strokeText('Appuyer sur Espace pour rejouer', width / 2, height - 500);
    ctx.fillText('Appuyer sur Espace pour rejouer', width / 2, height - 500);
}
