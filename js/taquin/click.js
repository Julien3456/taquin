/****************************************************************************************
 * Classe position du click
 * @param {Number} x - position X
 * @param {Number} y - Position Y
 */
class Click {

    _section
    move;
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
    get section() {
        return this._section;
    }

    /****************************************************************************************
     * Fonction permettant de déterminer la direction de la section vide et la distance
     * @return {void}
     */
    whereEmpty() {
        let result;
        if (Number.isInteger((settings.emptySection.id - this._section.id) / settings.level)) {
            result = { dir: 'y', dist: ((settings.emptySection.id - this._section.id) / settings.level) };
        }

        if (Math.floor(settings.emptySection.id / settings.level) ===
            Math.floor(this._section.id / settings.level)) {
            result = { dir: 'x', dist: (settings.emptySection.id - this._section.id) };
        }
        this.move = result;
    }


    /****************************************************************************************
     * Fonction recherche de la section clickée
     * @returns {void}
     */
    findPos(){
        this._section = settings.tabSections.find(section =>
            (this.x >= section.sx && this.x < (section.sx + section.sWidth)) &&
            (this.y >= section.sy && this.y < (section.sy + section.sHeight))
        )
    }
}