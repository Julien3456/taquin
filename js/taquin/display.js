class display {
    thumbnail = document.getElementById('thumbnail');
    image;
    tabZones = [];

    constructor() {
        this.width = 800;
        this.height = 800;
        this.canvas = this.createCanvas();
        this.context = this.canvas.getContext('2d');
        this.ratio = this.width / this.canvas.clientWidth ;
    }

    /****************************************************************************************
     * Fonction de création du canvas
     * @returns {HTMLElement}
     */
    createCanvas() {
        let canvas = document.getElementById('canvas');
        canvas.width = 800;
        canvas.height = 800;
        return canvas;
    }

    /****************************************************************************************
     * Fonction modification de l'affichage
     * @returns {void}
     */
    modifyDisplay(){
        this.context.clearRect(0, 0, this.width, this.height);
        this.prepareContext(this.image);
    }

    /****************************************************************************************
     * Fonction d'affichage de l'image du taquin par sections
     * @returns {void}
     */
    displayTaquin(){
        settings.getTaquinPicture().then(async (image) => {
            this.image = image
            this.displayThumbnail(this.image);
            this.prepareContext(image);
        })
    }

    /****************************************************************************************
     * Fonction de préparation du context
     * @param {HTMLImageElement} image - Image du taquin
     * @returns {void}
     */
    prepareContext(image){
        let tab = [];
        settings.tabSections.forEach((section, i) => {
            this.context.beginPath();
            this.displaySectionTaquin(image, section.idZone, i);
            if (section.id === section.idZone) tab.push(section);
        })
        this.displayFrame(settings.tabSections, "white");
        if (settings.help) this.displayFrame(tab, "green");
        this.context.closePath()
    }

    /****************************************************************************************
     * Fonction d'affichage de chaque section de la ligne de l'image du taquin
     * @param {HTMLImageElement} image - Image du taquin
     * @param {Number} idZone - Indice de la zone à afficher
     * @param {Number} index - Indice de la section sur le taquin
     * @returns {void}
     */
    displaySectionTaquin(image, idZone, index){
        if (settings.tabSections[idZone].id  !== settings.nbSections - 1) {
            this.context.drawImage(
                image,
                settings.tabSections[idZone].sx,
                settings.tabSections[idZone].sy,
                settings.tabSections[idZone].sWidth,
                settings.tabSections[idZone].sHeight,
                this.tabZones[index].x,
                this.tabZones[index].y,
                settings.tabSections[idZone].sWidth,
                settings.tabSections[idZone].sHeight
            );
        }
    }

    /****************************************************************************************
     * Fonction d'affichage de la Thumbnail du taquin
     * @param {Array<Section>} tab - Section à quadriller
     * @param {string} color - couleur du cadre
     * @returns {void}
     */
    displayFrame(tab, color){
        tab.forEach(section => {
            this.context.lineWidth = 3;
            this.context.strokeStyle = color;
            this.context.strokeRect(
                section.sx,
                section.sy,
                section.sWidth,
                section.sHeight,
            );
        })
    }

    /****************************************************************************************
     * Fonction d'affichage de la Thumbnail du taquin
     * @returns {void}
     */
    displayThumbnail(image){
        this.thumbnail.src = image.src;
        this.thumbnail.alt = "Thumbnail du taquin";
    }

    /****************************************************************************************
     * Fonction d'affichage du texte de fin de partie
     * @returns {void}
     */
    displayWin(){
        this.context.font = 'bold 64px sans-serif';
        this.context.fillStyle = '#000';
        this.context.textAlign = 'center';
        this.context.textBaseline = 'middle';
        this.context.strokeStyle = 'white';
        this.context.lineWidth = 5;
        this.context.strokeText('Retour à la normal', this.width / 2, this.height - 600);
        this.context.fillText('Retour à la normal', this.width / 2, this.height - 600);
        this.context.font = 'bold 30px sans-serif';
        this.context.strokeText('Appuyer sur Espace pour rejouer', this.width / 2, this.height - 500);
        this.context.fillText('Appuyer sur Espace pour rejouer', this.width / 2, this.height - 500);
    }
}
