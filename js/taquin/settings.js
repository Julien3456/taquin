/****************************************************************************************
 * Classe Settings pour les paramètres du jeux
 * @param {display} display - Objet display
 * @param {Number} level - Niveau de difficulté séléctionné par le joueur
 * @param {Boolean} move - Propriété grand déplacement
 * @param {Boolean} help - Propriété d'aide au positionnement
 */
class Settings{
    widthSection = 0;
    heightSection = 0;
    tabSections = [];
    emptySection;
    #start = false;
    constructor(display, level = 3, move = false, help = false) {
        this.level = level;
        this.move = move;
        this.help = help;
        this.nbSections = this.getNbSection(this.level);
        this.urlPicture = this.findUrlPicture();
    }

    get start() {
        return this.#start;
    }

    set start(value) {
        this.#start = value;
    }

    /****************************************************************************************
     * Fonction rechercher des images aléatoirement
     * @returns {string}
     */
    findUrlPicture(){
        let nbImg = Math.floor((Math.random() * 10)+1);
        return `images/gamePictures/image${nbImg}.jpg`;
    }

    /****************************************************************************************
     * Fonction de chargement de l'image avec l'url de la fonction "findUrlPicture()"
     * @returns {Promise<HTMLImageElement>}
     */
    getTaquinPicture() {
        return new Promise((loader) => {
            const picture = new Image();
            picture.src = this.urlPicture;
            picture.onload = () => loader(picture)
        })
    }

    /****************************************************************************************
     * Fonction Setter de la propriété Level
     * @param {Number} choice - Niveau de difficulté séléctionné par le joueur
     * @returns {void}
     */
    setLevel(choice){
        this.level = choice;
        this.nbSections = this.getNbSection(choice);
    }

    /****************************************************************************************
     * Fonction Setter de la propriété nbSection
     * @param {Number} nb - Niveau de difficulté séléctionné par le joueur
     * @returns {Number}
     */
    getNbSection(nb){
        return Math.pow(this.level, 2);
    }

    /****************************************************************************************
     * Fonction Setter de la propriété grand déplacement
     * @param {Boolean} choice - Choix de la propriété de l'option grand déplacement
     * @returns {void}
     */
    setMove(choice){
        this.move = choice;
    }

    /****************************************************************************************
     * Fonction Setter de la propriété d'aide au positionnement
     * @param {Boolean} choice - Choix de la propriété de l'option d'aide au positionnement
     * @returns {void}
     */
    setHelp(choice){
        this.help = choice;
    }

    /*******************************************************************************************************
     * Fonction générant des nombres aléatoirement entre 0 et le max en paramètre
     * @returns {Array<Number>}
     */
    mixSection(){
        let numSection;
        let tab = [];
        for (let i = 0; i < settings.nbSections -1; i++){
            do {
                numSection = Math.floor(Math.random() * (settings.nbSections -1)) + 1;
            }
            while (tab.includes(numSection));
            tab.push(numSection);
            this.tabSections[i].idZone = numSection -1;
        }
        this.tabSections[settings.nbSections -1].idZone = settings.nbSections -1;
    }

    /**********************************************************************************************************
     * fonction retournant une variable indiquant le mélangeomètre
     * @param {array<Section>} tab - tableau de section brouillé
     * @param {Result} result - La classe result pour stocker le resultat
     * @returns {void}
     */
    mixDegree(tab, result){
        let counter = 0;
        for (let i = 0; i < this.nbSections - 1; i++ ){
            if (tab[i].idZone > tab[i+1].idZone && tab[i].idZone !== this.nbSections - 1 && tab[i+1].idZone !== this.nbSections - 1){
                counter++
            }
        }
        result.mixometer = counter;
    }

    /*******************************************************************************************************
     * Fonction permettant d'activer ou désactiver les niveaux de difficultés
     * @param {boolean} disable - paramètre indiquant si les options sont actives
     * @returns {void}
     */
    disableDifficulty(disable){
        const elements = document.getElementsByClassName('difficulty-option');
        for (let i = 0; i < elements.length; i++) {
            if (disable) {
                elements[i].classList.add('disable');
                elements[i].children[0].setAttribute('disabled', true);
            } else {
                elements[i].classList.remove('disable');
                elements[i].children[0].removeAttribute('disabled');
            }
        }
    }

    /*******************************************************************************************************
     * Fonction permettant de réinitiliser le jeux
     * @returns {void}
     */
    init(){
        this.start = false;
        document.getElementsByClassName('difficulty-option')[0].children[0].checked = true;
        document.getElementById('toggle1').checked = false;
        document.getElementById('toggle2').checked = false;
    }
}