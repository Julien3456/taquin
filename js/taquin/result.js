class Result {
    constructor(counter, mixometer) {
        this.counter = counter;
        this._mixometer = mixometer;
    }
    get mixometer() {
        return this._mixometer;
    }

    set mixometer(value) {
        this._mixometer = value;
    }

    addClick(){
        this.counter += 1;
    }

    showResult(){
        document.getElementById('mix').value = this.mixometer;
        document.getElementById('click').value = result.counter;
    }
}