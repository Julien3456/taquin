/****************************************************************************************
 * Classe Gérant les différentes sections de l'image du taquin
 * @param {Number} id - Id de section
 * @param {Number} sx - Emplacement de l'angle supérieur gauche sur l'axe X de l'image source
 * @param {Number} sy - Emplacement de l'angle supérieur gauche sur l'axe Y de l'image source
 * @param {Number} sWidth - Largeur de la section
 * @param {Number} sHeight - Hauteur de la section
 * @param {Number} idZone - L'emplacement sur le taquin
 */
class Section {
    constructor(id, sx, sy, idZone,  sWidth = settings.widthSection, sHeight = settings.heightSection) {
        this.id = id;
        this.sx = sx;
        this.sy = sy;
        this.sWidth = sWidth;
        this.sHeight = sHeight;
        this.idZone = idZone;
    }

    /****************************************************************************************
     * Fonction création du tableau de sections
     * @param {display} picture - Objet image du taquin
     * @returns {void}
     */
    static createTabSections(picture){
        settings.widthSection = picture.width / settings.level;
        settings.heightSection = picture.height / settings.level;
        let tabSections = [];
        let tabZones = [];
        for (let i=0; i < settings.nbSections; i++) {
            let sx = (i%settings.level)*settings.widthSection;
            let sy = Math.floor(i/settings.level)*settings.heightSection;
            let section = new Section(i, sx, sy, i);
            tabSections.push(section);
            tabZones.push({x: sx, y: sy});
        }
        settings.emptySection = tabSections[settings.nbSections -1];
        settings.tabSections = tabSections;
        picture.tabZones = tabZones;
    }

    /****************************************************************************************
     * Fonction permettant de filtrer l'option grande distance
     * @param {Click} click - Section selectionnée
     * @returns {void}
     */
    move(click){
        click.whereEmpty();
        let tabSections = [];

        if ((settings.move === false && Math.abs(click.move.dist) === 1) || (settings.move === true && Math.abs(click.move.dist) >= 1)) {
            tabSections = this.findZonesDest(click);
            this.moveX(tabSections, click);
        }
    }

    /****************************************************************************************
     * Fonction permettant de créer un tableau de sections à déplacer
     * @param {Click} click - Section selectionnée
     * @returns {Array<Section>}
     */
    findZonesDest(click){
        let tabSectionsMove = [];
        let sign = Math.sign(click.move.dist);

        if (click.move.dir === 'x') {
            for (let i = Math.abs(click.move.dist); i >= 1; i--) {
                tabSectionsMove.push(settings.tabSections[click._section.id + (i * sign)]);
            }
        }

        if (click.move.dir === 'y') {
            for (let i = Math.abs(click.move.dist); i >= 1 ; i--){
                tabSectionsMove.push(settings.tabSections[click._section.id + ( settings.level * (i * sign))]);
            }
        }
        tabSectionsMove.push(click._section);
        return tabSectionsMove;
    }

    /****************************************************************************************
     * Fonction permettant de créer un tableau de sections à déplacer
     * @param {Array<Section>} tabSectionsDest - Tableau de sections à déplacer
     * @param {Click} click - Objet du click
     * @returns {void}
     */
    moveX(tabSectionsDest, click){
        for (let i = 1; i < tabSectionsDest.length; i++) {
            let idZoneDest = tabSectionsDest[i].idZone;
            settings.tabSections[tabSectionsDest[i].id].idZone = tabSectionsDest[i-1].idZone;
            settings.tabSections[tabSectionsDest[i-1].id].idZone = idZoneDest;
        }
        settings.emptySection = settings.tabSections[click._section.id];
    }
}